package main

import (
	"fmt"
	"sync"
	"time"
	"net/http"
	"encoding/json"
)

type RespostaAPI struct {
	Err  error
	Data []byte
	API  string
	Time time.Duration
}

func pegaAPI(url string, nomeAPI string, ch chan<- RespostaAPI, wg *sync.WaitGroup) {
	defer wg.Done()
	cliente := http.Client{
		Timeout: time.Second,
	}
	iniciaResposta := time.Now()
	resposta, err := cliente.Get(url)
	duration := time.Since(iniciaResposta)
	if err != nil {
		ch <- RespostaAPI{Err: fmt.Errorf("%s erro ou expiracao: %w", nomeAPI, err)}
		return
	}
	defer resposta.Body.Close()
	var data map[string]interface{}
	err = json.NewDecoder(resposta.Body).Decode(&data)
	if err != nil {
		ch <- RespostaAPI{Err: fmt.Errorf("%s erro decode: %w", nomeAPI, err)}
		return
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		ch <- RespostaAPI{Err: fmt.Errorf("%s erro marchal: %w", nomeAPI, err)}
		return
	}
	ch <- RespostaAPI{Data: jsonData, API: nomeAPI, Time: duration}
}

func main() {
	cep := "01153000"
	ch := make(chan RespostaAPI, 2)
	var wg sync.WaitGroup
	wg.Add(2)

	brasilAPI := fmt.Sprintf("https://brasilapi.com.br/api/cep/v1/%s", cep)
	viaCEP := fmt.Sprintf("http://viacep.com.br/ws/%s/json/", cep)

	go pegaAPI(viaCEP, "ViaCEP", ch, &wg)
	go pegaAPI(brasilAPI, "BrasilAPI", ch, &wg)

	wg.Wait()

	close(ch)

	var fResp RespostaAPI

	select {
	case fResp = <-ch:
	case <-time.After(time.Second):
		fmt.Println("Tempo expirado")
		return
	}

	if fResp.Err != nil {
		fmt.Printf("Falha mais rapida: %v\n", fResp.Err)
	} else {
		rapAPI := fResp.API
		rapMil := fResp.Time.Milliseconds()
		rapDat := string(fResp.Data)
		fmt.Printf("Resposta mais rapida: %s (%v ms):\n%s\n", rapAPI, rapMil, rapDat)
	}
}
